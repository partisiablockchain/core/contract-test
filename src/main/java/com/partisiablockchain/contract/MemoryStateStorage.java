package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateStorage;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Fake {@link StateStorage} that stores all written objects on the Java heap.
 *
 * <p><b>Should only be used in testing, not in production</b>
 *
 * <p>Possess some spy functionality, to verify behaviour of other components.
 */
public final class MemoryStateStorage implements StateStorage {

  private final Map<Hash, byte[]> storage = new HashMap<>();

  @Override
  public boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    if (storage.containsKey(hash)) {
      return false;
    }
    storage.put(
        hash,
        SafeDataOutputStream.serialize(
            t -> ExceptionConverter.run(() -> writer.accept(t), "Error writing to storage")));
    return true;
  }

  @Override
  public <SerializeT> SerializeT read(Hash hash, Function<SafeDataInputStream, SerializeT> reader) {
    if (storage.containsKey(hash)) {
      SafeDataInputStream bytes = SafeDataInputStream.createFromBytes(storage.get(hash));
      return ExceptionConverter.call(() -> reader.apply(bytes), "Error reading from storage");
    } else {
      return null;
    }
  }
}
