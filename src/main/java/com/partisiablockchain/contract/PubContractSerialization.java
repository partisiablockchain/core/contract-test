package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateStorage;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Manages a public contract in testing environment setting up elements to make the tests
 * comprehensive.
 *
 * @param <T> the state type for the contract.
 */
@SuppressWarnings("WeakerAccess")
public final class PubContractSerialization<T extends StateSerializable> {

  private final PubContract<T> pubContract;
  private final Class<T> contractStateType;
  private final StateStorage stateStorage = new MemoryStateStorage();

  /**
   * Creates a PubContract serialization test interface.
   *
   * @param <S> type of contract state
   * @param contractClass class of contract
   * @param contractStateClass class of contract state
   */
  public <S extends PubContract<T>> PubContractSerialization(
      Class<S> contractClass, Class<T> contractStateClass) {
    this.contractStateType = contractStateClass;
    this.pubContract =
        ExceptionConverter.call(
            () -> contractClass.getDeclaredConstructor().newInstance(),
            "Unable to instantiate contract");
  }

  /**
   * Deploy contract.
   *
   * @param context context for the current execution
   * @param rpc the invocation data
   * @return state of contract after creation
   */
  public T create(PubContractContext context, Consumer<SafeDataOutputStream> rpc) {
    byte[] serialize = SafeDataOutputStream.serialize(rpc);
    T contractState = call(serialize, input -> pubContract.onCreate(context, input));
    return verifySerializable(contractState);
  }

  /**
   * Perform invocation on contract.
   *
   * @param context context for the current invocation
   * @param contractState state for the contract
   * @param rpc the invocation data
   * @return state of contract after invocation
   */
  public T invoke(PubContractContext context, T contractState, Consumer<SafeDataOutputStream> rpc) {
    verifySerializable(contractState);
    byte[] serialize = SafeDataOutputStream.serialize(rpc);
    T newState = call(serialize, input -> pubContract.onInvoke(context, contractState, input));
    return verifySerializable(newState);
  }

  private T call(byte[] serialize, Function<SafeDataInputStream, T> invoke) {
    return SafeDataInputStream.readFully(serialize, invoke);
  }

  /**
   * Perform callback on contract.
   *
   * @param context context for the current invocation
   * @param callbackContext callback context for current invocation
   * @param contractState state for the contract
   * @param rpc the invocation data
   * @return state of contract after invocation
   */
  public T callback(
      PubContractContext context,
      CallbackContext callbackContext,
      T contractState,
      Consumer<SafeDataOutputStream> rpc) {
    verifySerializable(contractState);
    SysContractSerialization.verifyCallbackContextPreconditions(callbackContext);
    byte[] serialize = SafeDataOutputStream.serialize(rpc);
    T newState =
        call(
            serialize,
            input -> pubContract.onCallback(context, contractState, callbackContext, input));
    SysContractSerialization.verifyCallbackContextPostconditions(callbackContext);
    return verifySerializable(newState);
  }

  private T verifySerializable(T contractState) {
    return StateSerializableChecker.assertStateSerializable(
        stateStorage, contractState, contractStateType);
  }
}
