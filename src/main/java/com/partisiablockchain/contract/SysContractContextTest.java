package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.sys.DeployBuilder;
import com.partisiablockchain.contract.sys.DeployBuilderImpl;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.Governance;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.contract.sys.UpgradeBuilder;
import com.partisiablockchain.contract.sys.UpgradeBuilderImpl;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;

/**
 * Test implementation of {@link SysContractContext} with fixed values allowing for deterministic
 * testing.
 */
public final class SysContractContextTest implements SysContractContext {

  private final BlockchainAddress from;
  private final BlockchainAddress contractAddress;
  private final long productionTime;
  private final long blockTime;
  private final Hash transactionHash;
  private final Hash originalTransactionHash;

  private final List<BlockchainAddress> createdAccount;
  private final List<String> shards;
  private final List<LocalPluginStateUpdate> updateLocalAccountPluginStates;

  private AvlTree<String, String> features;

  private StateSerializable globalAccountPluginState;
  private PluginInteractionCreator accountPluginInteractions;
  private StateSerializable globalConsensusPluginState;
  private PluginInteractionCreator consensusPluginInteractions;
  private StateSerializable globalSharedObjectStorePluginState;
  private Governance governance;
  private long serviceFeesGas;
  private BlockchainAddress serviceFeesTarget;
  private long infrastructureFeesGas;
  private BlockchainAddress infrastructureFeesTarget;

  private Unsigned256 byocFeesGas;
  private String byocSymbol;
  private List<BlockchainAddress> byocNodes;

  private byte[] updateContextFreeAccountPluginState;
  private byte[] updateAccountPluginRpc;
  private byte[] updateAccountPluginJar;
  private GlobalPluginStateUpdate updateGlobalAccountPluginState;
  private GlobalPluginStateUpdate updateGlobalConsensusPluginState;
  private LocalPluginStateUpdate updateLocalConsensusPluginState;
  private GlobalPluginStateUpdate updateGlobalSharedObjectStorePluginState;
  private byte[] updateConsensusPluginJar;
  private byte[] updateConsensusPluginRpc;
  private byte[] updateRoutingPluginJar;
  private byte[] updateRoutingPluginRpc;

  private byte[] updateSharedObjectStorePluginJar;
  private byte[] updateSharedObjectStorePluginRpc;
  private BlockchainAddress removeContract;
  private BlockchainAddress exists;
  private byte[] upgradeSystemContractContractJar;
  private byte[] upgradeSystemContractBinderJar;
  private byte[] upgradeSystemContractAbi;
  private byte[] upgradeSystemContractRpc;
  private BlockchainAddress upgradeSystemContractContractAddress;
  private String shardId;

  private final List<ContractEvent> invocations;
  private ContractEventGroup remoteCalls;
  private byte[] result;

  /** Creates a new PubBinderContext with default values. */
  public SysContractContextTest() {
    this(
        System.currentTimeMillis(),
        77L,
        BlockchainAddress.fromString("000070000000000000000000000000000000000001"));
  }

  /**
   * Creates a new PubBinderContext.
   *
   * @param productionTime the human production time in millis
   * @param blockTime the block time count
   * @param from sender of the transaction
   */
  public SysContractContextTest(long productionTime, long blockTime, BlockchainAddress from) {
    this.productionTime = productionTime;
    this.blockTime = blockTime;
    this.from = from;
    contractAddress = BlockchainAddress.fromString("010070000000000000000000000000000000000001");
    transactionHash = Hash.create(stream -> stream.writeLong(productionTime));
    originalTransactionHash = Hash.create(stream -> stream.writeLong(blockTime));
    features = AvlTree.create();
    createdAccount = new ArrayList<>();
    shards = new ArrayList<>();
    updateLocalAccountPluginStates = new ArrayList<>();
    invocations = new ArrayList<>();
    byocNodes = new ArrayList<>();
  }

  @Override
  public BlockchainAddress getContractAddress() {
    return contractAddress;
  }

  @Override
  public SystemEventCreator getInvocationCreator() {
    SystemEventCreatorTest eventManager = new SystemEventCreatorTest(invocations);
    return eventManager;
  }

  @Override
  public SystemEventManager getRemoteCallsCreator() {
    if (remoteCalls == null) {
      remoteCalls = new ContractEventGroup();
    }
    SystemEventManager eventManager = new SystemEventManagerTest(remoteCalls);
    return eventManager;
  }

  @Override
  public void setResult(DataStreamSerializable result) {
    this.result = SafeDataOutputStream.serialize(result);
  }

  /**
   * Gets the result of the execution.
   *
   * @return the event group
   */
  public byte[] getResult() {
    return result;
  }

  /**
   * Gets the event group from the call back structure.
   *
   * @return the event group
   */
  public ContractEventGroup getRemoteCalls() {
    if (result != null) {
      return null;
    } else {
      return remoteCalls;
    }
  }

  /**
   * Gets the list of events.
   *
   * @return the events
   */
  public List<ContractEvent> getInteractions() {
    return List.copyOf(invocations);
  }

  @Override
  public StateSerializable getGlobalAccountPluginState() {
    return globalAccountPluginState;
  }

  /**
   * Sets the globalAccountPluginState.
   *
   * @param globalAccountPluginState the interaction
   */
  public void setGlobalAccountPluginState(StateSerializable globalAccountPluginState) {
    this.globalAccountPluginState = globalAccountPluginState;
  }

  @Override
  public PluginInteractionCreator getAccountPluginInteractions() {
    return accountPluginInteractions;
  }

  /**
   * Sets the accountPluginInteractions.
   *
   * @param accountPluginInteractions the interaction
   */
  public void setAccountPluginInteractions(PluginInteractionCreator accountPluginInteractions) {
    this.accountPluginInteractions = accountPluginInteractions;
  }

  @Override
  public StateSerializable getGlobalConsensusPluginState() {
    return globalConsensusPluginState;
  }

  /**
   * Sets the globalConsensusPluginState.
   *
   * @param globalConsensusPluginState the interaction
   */
  public void setGlobalConsensusPluginState(StateSerializable globalConsensusPluginState) {
    this.globalConsensusPluginState = globalConsensusPluginState;
  }

  @Override
  public PluginInteractionCreator getConsensusPluginInteractions() {
    return consensusPluginInteractions;
  }

  /**
   * Sets the consensusPluginInteractions.
   *
   * @param consensusPluginInteractions the interaction
   */
  public void setConsensusPluginInteractions(PluginInteractionCreator consensusPluginInteractions) {
    this.consensusPluginInteractions = consensusPluginInteractions;
  }

  @Override
  public StateSerializable getGlobalSharedObjectStorePluginState() {
    return globalSharedObjectStorePluginState;
  }

  /**
   * Sets the globalSharedObjectStorePluginState.
   *
   * @param globalSharedObjectStorePluginState the state
   */
  public void setGlobalSharedObjectStorePluginState(
      StateSerializable globalSharedObjectStorePluginState) {
    this.globalSharedObjectStorePluginState = globalSharedObjectStorePluginState;
  }

  @Override
  public Governance getGovernance() {
    return governance;
  }

  /**
   * Sets the governance.
   *
   * @param governance the new governance.
   */
  public void setGovernance(Governance governance) {
    this.governance = governance;
  }

  @Override
  public String getFeature(String key) {
    return features.getValue(key);
  }

  @Override
  public void registerDeductedByocFees(
      Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes) {
    this.byocFeesGas = amount;
    this.byocSymbol = symbol;
    this.byocNodes = new ArrayList<>(nodes);
  }

  /**
   * Get the fee amount related to a BYOC transaction.
   *
   * @return the fee amount.
   */
  public Unsigned256 getByocFeesGas() {
    return byocFeesGas;
  }

  /**
   * Get the BYOC coin symbol from a BYOC transaction.
   *
   * @return the BYOC coin symbol.
   */
  public String getByocSymbol() {
    return byocSymbol;
  }

  /**
   * Get the small oracle nodes that were used in a BYOC transaction.
   *
   * @return the nodes.
   */
  public List<BlockchainAddress> getByocNodes() {
    return byocNodes;
  }

  @Override
  public long getBlockTime() {
    return blockTime;
  }

  @Override
  public long getBlockProductionTime() {
    return productionTime;
  }

  @Override
  public BlockchainAddress getFrom() {
    return from;
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return transactionHash;
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return originalTransactionHash;
  }

  @Override
  public void payServiceFees(long gas, BlockchainAddress target) {
    this.serviceFeesGas = gas;
    this.serviceFeesTarget = target;
  }

  @Override
  public void payInfrastructureFees(long gas, BlockchainAddress target) {
    this.infrastructureFeesGas = gas;
    this.infrastructureFeesTarget = target;
  }

  /**
   * The gas used to pay for service.
   *
   * @return the gas
   */
  public long getServiceFeesGas() {
    return serviceFeesGas;
  }

  /**
   * The target used to pay for service.
   *
   * @return the target
   */
  public BlockchainAddress getServiceFeesTarget() {
    return serviceFeesTarget;
  }

  /**
   * The gas used to pay for infrastructure.
   *
   * @return the gas
   */
  public long getInfrastructureFeesGas() {
    return infrastructureFeesGas;
  }

  /**
   * The target used to pay for infrastructure.
   *
   * @return the target
   */
  public BlockchainAddress getInfrastructureFeesTarget() {
    return infrastructureFeesTarget;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateLocalAccountPluginState.
   */
  public List<LocalPluginStateUpdate> getUpdateLocalAccountPluginStates() {
    return updateLocalAccountPluginStates;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateContextFreeAccountPluginState.
   */
  public byte[] getUpdateContextFreeAccountPluginState() {
    return updateContextFreeAccountPluginState;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateAccountPluginRpc.
   */
  public byte[] getUpdateAccountPluginRpc() {
    return updateAccountPluginRpc;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateAccountPluginJar.
   */
  public byte[] getUpdateAccountPluginJar() {
    return updateAccountPluginJar;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateGlobalAccountPluginState.
   */
  public GlobalPluginStateUpdate getUpdateGlobalAccountPluginState() {
    return updateGlobalAccountPluginState;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateGlobalConsensusPluginState.
   */
  public GlobalPluginStateUpdate getUpdateGlobalConsensusPluginState() {
    return updateGlobalConsensusPluginState;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateLocalConsensusPluginState.
   */
  public LocalPluginStateUpdate getUpdateLocalConsensusPluginState() {
    return updateLocalConsensusPluginState;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateGlobalSharedObjectStorePluginState.
   */
  public GlobalPluginStateUpdate getUpdateGlobalSharedObjectStorePluginState() {
    return updateGlobalSharedObjectStorePluginState;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateConsensusPluginJar.
   */
  public byte[] getUpdateConsensusPluginJar() {
    return updateConsensusPluginJar;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateConsensusPluginRpc.
   */
  public byte[] getUpdateConsensusPluginRpc() {
    return updateConsensusPluginRpc;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateRoutingPluginJar.
   */
  public byte[] getUpdateRoutingPluginJar() {
    return updateRoutingPluginJar;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateRoutingPluginRpc.
   */
  public byte[] getUpdateRoutingPluginRpc() {
    return updateRoutingPluginRpc;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateSharedObjectStorePluginJar.
   */
  public byte[] getUpdateSharedObjectStorePluginJar() {
    return updateSharedObjectStorePluginJar;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the updateSharedObjectStorePluginRpc.
   */
  public byte[] getUpdateSharedObjectStorePluginRpc() {
    return updateSharedObjectStorePluginRpc;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the removeContract.
   */
  public BlockchainAddress getRemoveContract() {
    return removeContract;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the exists.
   */
  public BlockchainAddress getExists() {
    return exists;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the upgradeSystemContractContractJar.
   */
  public byte[] getUpgradeSystemContractContractJar() {
    return upgradeSystemContractContractJar;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the upgradeSystemContractBinderJar.
   */
  public byte[] getUpgradeSystemContractBinderJar() {
    return upgradeSystemContractBinderJar;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the upgradeSystemContractAbi.
   */
  public byte[] getUpgradeSystemContractAbi() {
    return upgradeSystemContractAbi;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the upgradeSystemContractRpc.
   */
  public byte[] getUpgradeSystemContractRpc() {
    return upgradeSystemContractRpc;
  }

  /**
   * Gets the result of calling the event manager.
   *
   * @return the upgradeSystemContractContractAddress.
   */
  public BlockchainAddress getUpgradeSystemContractContractAddress() {
    return upgradeSystemContractContractAddress;
  }

  /**
   * The account created.
   *
   * @return the account
   */
  public List<BlockchainAddress> getCreatedAccount() {
    return createdAccount;
  }

  /**
   * Gets the active shards.
   *
   * @return the shards
   */
  public List<String> getShards() {
    return shards;
  }

  /**
   * The shard id used in updateContextFreeAccountPluginState.
   *
   * @return the shard id
   */
  public String getShardId() {
    return shardId;
  }

  /** Test event creator. */
  abstract class SystemEventAbstractTest implements SystemEventCreator {

    private final List<ContractEvent> invocations;
    private final EventCreatorImpl eventCreator;

    /**
     * Creates a new event creator.
     *
     * @param invocations the destination for new invocations.
     */
    public SystemEventAbstractTest(List<ContractEvent> invocations) {
      this.invocations = invocations;
      this.eventCreator = new EventCreatorImpl(invocations::add);
    }

    @Override
    public void createAccount(BlockchainAddress address) {
      createdAccount.add(address);
    }

    @Override
    public void createShard(String shardId) {
      shards.add(shardId);
    }

    @Override
    public void removeShard(String shardId) {
      shards.remove(shardId);
    }

    @Override
    public void updateLocalAccountPluginState(LocalPluginStateUpdate update) {
      updateLocalAccountPluginStates.add(update);
    }

    @Override
    public void updateContextFreeAccountPluginState(String shardId, byte[] rpc) {
      SysContractContextTest.this.shardId = shardId;
      updateContextFreeAccountPluginState = rpc;
    }

    @Override
    public void updateGlobalAccountPluginState(GlobalPluginStateUpdate update) {
      updateGlobalAccountPluginState = update;
    }

    @Override
    public void updateAccountPlugin(byte[] pluginJar, byte[] rpc) {
      updateAccountPluginJar = pluginJar;
      updateAccountPluginRpc = rpc;
    }

    @Override
    public void updateLocalConsensusPluginState(LocalPluginStateUpdate update) {
      updateLocalConsensusPluginState = update;
    }

    @Override
    public void updateGlobalConsensusPluginState(GlobalPluginStateUpdate update) {
      updateGlobalConsensusPluginState = update;
    }

    @Override
    public void updateConsensusPlugin(byte[] pluginJar, byte[] rpc) {
      updateConsensusPluginJar = pluginJar;
      updateConsensusPluginRpc = rpc;
    }

    @Override
    public void updateRoutingPlugin(byte[] pluginJar, byte[] rpc) {
      updateRoutingPluginJar = pluginJar;
      updateRoutingPluginRpc = rpc;
    }

    @Override
    public void updateGlobalSharedObjectStorePluginState(GlobalPluginStateUpdate update) {
      updateGlobalSharedObjectStorePluginState = update;
    }

    @Override
    public void updateSharedObjectStorePlugin(byte[] pluginJar, byte[] rpc) {
      updateSharedObjectStorePluginJar = pluginJar;
      updateSharedObjectStorePluginRpc = rpc;
    }

    @Override
    public DeployBuilder deployContract(BlockchainAddress contract) {
      return new DeployBuilderImpl(invocations::add, contract);
    }

    @Override
    public UpgradeBuilder upgradeContract(BlockchainAddress contract) {
      return new UpgradeBuilderImpl(invocations::add, contract);
    }

    @Override
    public void removeContract(BlockchainAddress address) {
      removeContract = address;
    }

    @Override
    public void exists(BlockchainAddress address) {
      exists = address;
    }

    @Override
    public void setFeature(String key, String value) {
      features = features.set(key, value);
    }

    @Override
    public void upgradeSystemContract(
        byte[] contractJar, byte[] binderJar, byte[] rpc, BlockchainAddress contractAddress) {
      upgradeSystemContract(contractJar, binderJar, new byte[0], rpc, contractAddress);
    }

    @Override
    public void upgradeSystemContract(
        byte[] contractJar,
        byte[] binderJar,
        byte[] abi,
        byte[] rpc,
        BlockchainAddress contractAddress) {
      upgradeSystemContractContractJar = contractJar;
      upgradeSystemContractBinderJar = binderJar;
      upgradeSystemContractAbi = abi;
      upgradeSystemContractRpc = rpc;
      upgradeSystemContractContractAddress = contractAddress;
    }

    @Override
    public InteractionBuilder invoke(BlockchainAddress address) {
      return eventCreator.invoke(address);
    }
  }

  /** Test event creator. */
  public final class SystemEventCreatorTest extends SystemEventAbstractTest {

    SystemEventCreatorTest(List<ContractEvent> invocations) {
      super(invocations);
    }
  }

  /** Test event manager. */
  public final class SystemEventManagerTest extends SystemEventAbstractTest
      implements SystemEventManager {

    private final EventManagerImpl eventManager;

    SystemEventManagerTest(ContractEventGroup eventGroup) {
      super(eventGroup.contractEvents);
      this.eventManager = new EventManagerImpl(eventGroup);
    }

    @Override
    public void registerCallback(DataStreamSerializable rpc, long allocatedCost) {
      eventManager.registerCallback(rpc, allocatedCost);
    }

    @Override
    public void registerCallbackWithCostFromRemaining(DataStreamSerializable rpc) {
      eventManager.registerCallbackWithCostFromRemaining(rpc);
    }

    @Override
    public void registerCallbackWithCostFromContract(
        DataStreamSerializable rpc, long allocatedCost) {
      eventManager.registerCallbackWithCostFromContract(rpc, allocatedCost);
    }
  }
}
