package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;

/**
 * Specialty class for asserting the equality of two contract states ({@link StateSerializable}).
 *
 * <p>This class has special handling of certain classes that are often used in contract states, but
 * which cannot be directly handled by {@code usingRecursiveComparison}. {@code
 * usingRecursiveComparison} does not handle the fields of these classes correctly, due to
 * class-internal caching of values, resulting in spurious inequalities. The specially handled
 * classes are:
 *
 * <ul>
 *   <li>{@link BlockchainPublicKey}: Cryptographic primitive.
 *   <li>{@link BlsPublicKey}: Cryptographic primitive.
 *   <li>{@link BlsSignature}: Cryptographic primitive.
 *   <li>{@link AvlTree}: State datastructure with built-in caching.
 *   <li>{@link FixedList}: State datastructure with built-in caching.
 *   <li>{@link LargeByteArray}: State datastructure with built-in caching.
 * </ul>
 *
 * <p>Part of this was source-copied from <a
 * href="https://gitlab.com/partisiablockchain/core/contract">core/contract</a>.
 */
@CheckReturnValue
public final class StateSerializableEquality {

  private StateSerializableEquality() {}

  /**
   * Assert that the two given states are equal.
   *
   * @param state1 The first state.
   * @param state2 The second state.
   * @throws RuntimeException if the states are not equal.
   */
  public static void assertStatesEqual(StateSerializable state1, StateSerializable state2) {
    Assertions.assertThat(state1)
        .usingRecursiveComparison(ASSERTIONS_CONFIG_STATE_SERIALIZABLE)
        .isEqualTo(state2);
  }

  /** {@link RecursiveComparisonConfiguration} used to check that states are equal. */
  @SuppressWarnings("unchecked")
  public static final RecursiveComparisonConfiguration ASSERTIONS_CONFIG_STATE_SERIALIZABLE =
      RecursiveComparisonConfiguration.builder()
          .withEqualsForType(BlockchainPublicKey::equals, BlockchainPublicKey.class)
          .withEqualsForType(BlsPublicKey::equals, BlsPublicKey.class)
          .withEqualsForType(BlsSignature::equals, BlsSignature.class)
          .withEqualsForType(StateSerializableEquality::isEqualAvlTree, AvlTree.class)
          .withEqualsForType(StateSerializableEquality::isEqualFixedList, FixedList.class)
          .withEqualsForType(StateSerializableEquality::isEqualLargeByteArray, LargeByteArray.class)
          .withStrictTypeChecking(true)
          .build();

  /**
   * Custom equality between two AVL-trees.
   *
   * <p>They are equal if, they possess the same keyset, and are recursively equal.
   *
   * <p>Source copied from {@code core/contract} tests, and adjusted.
   *
   * @param <K> Type of keys.
   * @param <V> Type of values.
   * @param tree1 First tree to compare. Nullable.
   * @param tree2 Second tree to compare. Nullable.
   * @return true if equal.
   */
  static <K extends Comparable<K>, V> boolean isEqualAvlTree(
      final AvlTree<K, V> tree1, final AvlTree<K, V> tree2) {
    // Shortcut for different sizes
    if (tree1 == tree2) {
      return true;
    } else if (tree1 == null || tree2 == null) {
      return false;
    } else if (tree1.size() != tree2.size()) {
      return false;
    }

    final var keys = new LinkedHashSet<K>();
    keys.addAll(tree1.keySet());
    keys.addAll(tree2.keySet());
    try {
      for (final K key : keys) {
        Assertions.assertThat(tree1.getValue(key))
            .as(key.toString())
            .usingRecursiveComparison(ASSERTIONS_CONFIG_STATE_SERIALIZABLE)
            .isEqualTo(tree2.getValue(key));
      }
    } catch (AssertionError e) {
      return false;
    }
    return true;
  }

  /**
   * Custom equality between two fixed lists.
   *
   * <p>They are equal if, they are equally long, and are recursively equal.
   *
   * <p>Source copied from {@code core/contract} tests, and adjusted.
   *
   * @param <V> Type of values.
   * @param list1 First list to compare. Nullable.
   * @param list2 Second list to compare. Nullable.
   * @return true if equal.
   */
  static <V> boolean isEqualFixedList(final FixedList<V> list1, final FixedList<V> list2) {
    // Shortcut for different sizes
    if (list1 == list2) {
      return true;
    } else if (list1 == null || list2 == null) {
      return false;
    } else if (list1.size() != list2.size()) {
      return false;
    }

    try {
      for (int idx = 0; idx < list1.size(); idx++) {
        Assertions.assertThat(list1.get(idx))
            .as("" + idx)
            .usingRecursiveComparison(ASSERTIONS_CONFIG_STATE_SERIALIZABLE)
            .isEqualTo(list2.get(idx));
      }
    } catch (AssertionError e) {
      return false;
    }
    return true;
  }

  /**
   * Custom between large byte arrays.
   *
   * <p>They are equal if, they are equally long, and each bytes is pairwise equal.
   *
   * <p>Source copied from {@code core/contract} tests, and adjusted.
   *
   * @param array1 First array to compare
   * @param array2 Second array to compare.
   * @return true if equal.
   */
  static boolean isEqualLargeByteArray(final LargeByteArray array1, final LargeByteArray array2) {
    if (array1 == array2) {
      return true;
    } else if (array1 == null || array2 == null) {
      return false;
    }
    return Arrays.equals(array1.getData(), array2.getData());
  }
}
