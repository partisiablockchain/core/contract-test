package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Test implementation of {@link PubContractContext} with fixed values allowing for deterministic
 * testing.
 */
public final class PubContractContextTest implements PubContractContext {

  private final BlockchainAddress from;
  private final BlockchainAddress contractAddress;
  private final long productionTime;
  private final long blockTime;
  private final Hash transactionHash;
  private final Hash originalTransactionHash;

  private final List<ContractEvent> invocations;
  private ContractEventGroup remoteCalls;
  private byte[] result;

  /** Creates a new PubBinderContext with default values. */
  public PubContractContextTest() {
    this(
        System.currentTimeMillis(),
        77L,
        BlockchainAddress.fromString("000070000000000000000000000000000000000001"));
  }

  /**
   * Creates a new PubBinderContext.
   *
   * @param productionTime the human production time in millis
   * @param blockTime the block time count
   * @param from sender of the transaction
   */
  public PubContractContextTest(long productionTime, long blockTime, BlockchainAddress from) {
    this.productionTime = productionTime;
    this.blockTime = blockTime;
    this.from = from;
    contractAddress = BlockchainAddress.fromString("010070000000000000000000000000000000000001");
    transactionHash = Hash.create(stream -> stream.writeLong(productionTime));
    originalTransactionHash = Hash.create(stream -> stream.writeLong(blockTime));
    this.invocations = new ArrayList<>();
  }

  @Override
  public BlockchainAddress getContractAddress() {
    return contractAddress;
  }

  @Override
  public EventCreator getInvocationCreator() {
    EventCreatorImpl eventManager = new EventCreatorImpl(invocations::add);
    return eventManager;
  }

  @Override
  public EventManager getRemoteCallsCreator() {
    if (remoteCalls == null) {
      remoteCalls = new ContractEventGroup();
    }
    EventManagerImpl eventManager = new EventManagerImpl(remoteCalls);
    return eventManager;
  }

  @Override
  public void setResult(DataStreamSerializable result) {
    this.result = SafeDataOutputStream.serialize(result);
  }

  @Override
  public long getBlockTime() {
    return blockTime;
  }

  @Override
  public long getBlockProductionTime() {
    return productionTime;
  }

  @Override
  public BlockchainAddress getFrom() {
    return from;
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return transactionHash;
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return originalTransactionHash;
  }

  /**
   * Gets the result of the execution.
   *
   * @return the event group
   */
  public byte[] getResult() {
    return result;
  }

  /**
   * Gets the event group from the call back structure.
   *
   * @return the event group
   */
  public ContractEventGroup getRemoteCalls() {
    if (result != null) {
      return null;
    } else {
      return remoteCalls;
    }
  }

  /**
   * Gets the list of events.
   *
   * @return the events
   */
  public List<ContractEvent> getInteractions() {
    return List.copyOf(invocations);
  }
}
