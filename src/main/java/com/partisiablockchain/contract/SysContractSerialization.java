package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateStorage;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import org.assertj.core.api.SoftAssertions;

/**
 * Manages a system contract in testing environment setting up elements to make the tests
 * comprehensive.
 *
 * @param <T> the state type for the contract.
 */
@SuppressWarnings("WeakerAccess")
public final class SysContractSerialization<T extends StateSerializable> {

  private final SysContract<T> sysContract;
  private final Class<T> contractStateType;
  private final StateStorage stateStorage = new MemoryStateStorage();

  /**
   * Creates a SysContract serialization test interface.
   *
   * @param <S> type of contract state
   * @param contractClass class of contract
   * @param contractStateClass class of contract state
   */
  public <S extends SysContract<T>> SysContractSerialization(
      Class<S> contractClass, Class<T> contractStateClass) {
    this.contractStateType = contractStateClass;
    this.sysContract =
        ExceptionConverter.call(
            () -> contractClass.getDeclaredConstructor().newInstance(),
            "Unable to instantiate contract");
  }

  /**
   * Deploy contract.
   *
   * @param context context for the current execution
   * @param rpc the invocation data
   * @return state of contract after creation
   */
  public T create(SysContractContext context, Consumer<SafeDataOutputStream> rpc) {
    byte[] serialize = SafeDataOutputStream.serialize(rpc);
    T state = call(serialize, input -> sysContract.onCreate(context, input));
    return verifySerializable(state);
  }

  /**
   * Perform invocation on contract.
   *
   * @param context context for the current invocation
   * @param contractState state for the contract
   * @param rpc the invocation data
   * @return state of contract after invocation
   */
  public T invoke(SysContractContext context, T contractState, Consumer<SafeDataOutputStream> rpc) {
    verifySerializable(contractState);
    byte[] serialize = SafeDataOutputStream.serialize(rpc);
    T newState = call(serialize, input -> sysContract.onInvoke(context, contractState, input));
    return verifySerializable(newState);
  }

  private T call(byte[] serialize, Function<SafeDataInputStream, T> invoke) {
    return SafeDataInputStream.readFully(serialize, invoke);
  }

  /**
   * Perform callback on contract.
   *
   * @param context context for the current invocation
   * @param callbackContext callback context for current invocation
   * @param contractState state for the contract
   * @param rpc the invocation data
   * @return state of contract after invocation
   */
  public T callback(
      SysContractContext context,
      CallbackContext callbackContext,
      T contractState,
      Consumer<SafeDataOutputStream> rpc) {
    verifySerializable(contractState);
    verifyCallbackContextPreconditions(callbackContext);
    byte[] serialize = SafeDataOutputStream.serialize(rpc);
    T newState =
        call(
            serialize,
            input -> sysContract.onCallback(context, contractState, callbackContext, input));
    verifyCallbackContextPostconditions(callbackContext);
    return verifySerializable(newState);
  }

  private T verifySerializable(T contractState) {
    return StateSerializableChecker.assertStateSerializable(
        stateStorage, contractState, contractStateType);
  }

  /**
   * Verifies preconditions for callback context.
   *
   * <ul>
   *   <li>Execution results must have non-null return values, even failed ones.
   *   <li>Failed execution results must have an empty return value stream. Non-empty return value
   *       streams cannot occur when called on the blockchain.
   *   <li>Successful execution results must have distinct (object identity) return value streams.
   *       This is sometimes overzealous wrt. return-less callbacks.
   * </ul>
   *
   * @param callbackContext Callback context to verify.
   * @throws AssertionError If callback context is invalid wrt. preconditions.
   */
  public static void verifyCallbackContextPreconditions(CallbackContext callbackContext) {
    final SoftAssertions softly = new SoftAssertions();

    for (final CallbackContext.ExecutionResult result : callbackContext.results()) {
      softly
          .assertThat(result.returnValue())
          .as("CallbackContext.ExecutionResult#returnValue must always be non-null")
          .isNotNull();
      if (!result.isSucceeded() && result.returnValue() != null) {
        softly
            .assertThat(result.returnValue().readAllBytes())
            .as(
                "CallbackContext.ExecutionResult#returnValue#readAllBytes must be empty, when"
                    + " result failed")
            .isEmpty();
      }
    }

    final List<SafeDataInputStream> successfulReturnStreams =
        callbackContext.results().stream()
            .filter(CallbackContext.ExecutionResult::isSucceeded)
            .map(CallbackContext.ExecutionResult::returnValue)
            .toList();

    softly
        .assertThat(successfulReturnStreams)
        .as(
            "CallbackContext.ExecutionResult#returnValue streams must be distinct between all"
                + " successful execution results")
        .doesNotHaveDuplicates();

    if (!softly.errorsCollected().isEmpty()) {
      softly.fail(
          "CallbackContext preconditions failed: The test callback could never be received by a"
              + " contract on the blockchain; this might indicate flawed assumptions.");
    }
    softly.assertAll();
  }

  /**
   * Verifies postconditions for callback context.
   *
   * <ul>
   *   <li>All execution results of the callback context have been fully read. While restrictive, it
   *       ensures that tests actually follow the contract protocol.
   * </ul>
   *
   * @param callbackContext Callback context to verify.
   * @throws AssertionError If callback context is invalid wrt. postconditions.
   */
  public static void verifyCallbackContextPostconditions(CallbackContext callbackContext) {
    final SoftAssertions softly = new SoftAssertions();

    for (final CallbackContext.ExecutionResult result : callbackContext.results()) {
      softly
          .assertThat(result.returnValue().readAllBytes())
          .as("CallbackContext.ExecutionResult#returnValue must be fully read after callback")
          .isEmpty();
    }

    if (!softly.errorsCollected().isEmpty()) {
      softly.fail(
          "CallbackContext postconditions failed: Some execution result values have not been read;"
              + " this might indicate mismatched protocol between contract and tests.");
    }
    softly.assertAll();
  }
}
