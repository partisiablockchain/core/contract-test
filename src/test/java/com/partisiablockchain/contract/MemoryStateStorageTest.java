package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for {@link MemoryStateStorage}. */
public final class MemoryStateStorageTest {

  private final byte[] bytes = new byte[3];
  private final Hash hash = Hash.create(out -> out.write(bytes));

  @Test
  public void shouldOnlyWriteOnce() {
    MemoryStateStorage memoryStateStorage = new MemoryStateStorage();
    Assertions.assertThat(memoryStateStorage.write(hash, stream -> stream.write(bytes))).isTrue();
    Assertions.assertThat(memoryStateStorage.write(hash, stream -> stream.write(bytes))).isFalse();
  }

  @Test
  public void shouldReturnNullForNonExisting() {
    MemoryStateStorage memoryStateStorage = new MemoryStateStorage();
    Assertions.assertThat(memoryStateStorage.<byte[]>read(hash, stream -> bytes)).isNull();
    memoryStateStorage.write(hash, stream -> stream.write(bytes));
    Assertions.assertThat(memoryStateStorage.<byte[]>read(hash, stream -> bytes)).isEqualTo(bytes);
  }
}
