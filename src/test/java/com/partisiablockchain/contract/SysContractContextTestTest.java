package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.sys.ContractEventDeploy;
import com.partisiablockchain.contract.sys.ContractEventUpgrade;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test of the SysContractContextTest. */
public final class SysContractContextTestTest {

  private SysContractContextTest sysContext;
  private BlockchainAddress blockchainAddress;
  private byte[] rpc;

  /** Sets up the test. */
  @BeforeEach
  public void setUp() {
    this.sysContext = new SysContractContextTest();
    this.blockchainAddress =
        BlockchainAddress.fromString("000070000000000000000000000000000000000001");
    this.rpc = new byte[2];
  }

  @Test
  public void getContractAddress() {
    Assertions.assertThat(sysContext.getContractAddress())
        .isEqualTo(BlockchainAddress.fromString("010070000000000000000000000000000000000001"));
  }

  @Test
  public void getInvocationCreator() {
    SystemEventCreator eventManager = sysContext.getInvocationCreator();

    eventManager
        .deployContract(blockchainAddress)
        .withContractJar(rpc)
        .withBinderJar(rpc)
        .withAbi(rpc)
        .send();
    eventManager
        .upgradeContract(blockchainAddress)
        .withNewBinderJar(new byte[0])
        .withNewContractJar(new byte[0])
        .withNewAbi(new byte[0])
        .withUpgradeRpc(EventCreator.EMPTY_RPC)
        .send();
    eventManager.invoke(blockchainAddress).withPayload(blockchainAddress).send();
    Assertions.assertThat(sysContext.getInteractions()).hasSize(3);
    Assertions.assertThat(sysContext.getInteractions().get(0))
        .isInstanceOf(ContractEventDeploy.class);
    Assertions.assertThat(sysContext.getInteractions().get(1))
        .isInstanceOf(ContractEventUpgrade.class);
    Assertions.assertThat(sysContext.getInteractions().get(2))
        .isInstanceOf(ContractEventInteraction.class);
  }

  @Test
  public void getRemoteCallsCreator() {
    Assertions.assertThat(sysContext.getRemoteCalls()).isNull();
    SystemEventManager eventManager = sysContext.getRemoteCallsCreator();
    Assertions.assertThat(sysContext.getRemoteCalls()).isNotNull();
    Assertions.assertThat(sysContext.getResult()).isNull();
    Assertions.assertThat(eventManager)
        .isNotNull()
        .isInstanceOf(SysContractContextTest.SystemEventManagerTest.class);

    sysContext.getRemoteCallsCreator().registerCallback(EventManager.EMPTY_RPC, 1234);
    Assertions.assertThat(sysContext.getRemoteCalls().callbackCost).isEqualTo(1234);
    Assertions.assertThat(sysContext.getRemoteCalls().callbackRpc).isEqualTo(new byte[0]);

    eventManager.registerCallbackWithCostFromContract(EventManager.EMPTY_RPC, 4321);
    Assertions.assertThat(sysContext.getRemoteCalls().callbackCost).isEqualTo(4321);

    eventManager.registerCallbackWithCostFromRemaining(EventManager.EMPTY_RPC);
    Assertions.assertThat(sysContext.getRemoteCalls().callbackCost).isNull();
    Assertions.assertThat(sysContext.getRemoteCalls().contractEvents).isEmpty();

    eventManager
        .deployContract(blockchainAddress)
        .withContractJar(rpc)
        .withBinderJar(rpc)
        .withAbi(rpc)
        .send();
    eventManager
        .upgradeContract(blockchainAddress)
        .withNewBinderJar(new byte[0])
        .withNewContractJar(new byte[0])
        .withNewAbi(new byte[0])
        .withUpgradeRpc(EventManager.EMPTY_RPC)
        .send();
    eventManager.invoke(blockchainAddress).withPayload(blockchainAddress).send();
    Assertions.assertThat(sysContext.getRemoteCalls().contractEvents).hasSize(3);
    Assertions.assertThat(sysContext.getRemoteCalls().contractEvents.get(0))
        .isInstanceOf(ContractEventDeploy.class);
    Assertions.assertThat(sysContext.getRemoteCalls().contractEvents.get(1))
        .isInstanceOf(ContractEventUpgrade.class);
    Assertions.assertThat(sysContext.getRemoteCalls().contractEvents.get(2))
        .isInstanceOf(ContractEventInteraction.class);

    sysContext.setResult(s -> s.writeLong(1234));
    Assertions.assertThat(sysContext.getRemoteCalls()).isNull();
    Assertions.assertThat(sysContext.getResult())
        .isNotNull()
        .isEqualTo(new byte[] {0, 0, 0, 0, 0, 0, 4, -46});
  }

  @Test
  public void getGlobalAccountPluginState() {
    sysContext.setGlobalAccountPluginState(blockchainAddress);
    Assertions.assertThat(sysContext.getGlobalAccountPluginState()).isEqualTo(blockchainAddress);
  }

  @Test
  public void getAccountPluginInteractions() {
    PluginInteractionCreator accountPluginInteractions = new PluginInteractionCreator() {};
    sysContext.setAccountPluginInteractions(accountPluginInteractions);
    Assertions.assertThat(sysContext.getAccountPluginInteractions())
        .isEqualTo(accountPluginInteractions);
  }

  @Test
  public void getGlobalConsensusPluginState() {
    sysContext.setGlobalConsensusPluginState(blockchainAddress);
    Assertions.assertThat(sysContext.getGlobalConsensusPluginState()).isEqualTo(blockchainAddress);
  }

  @Test
  public void getConsensusPluginInteractions() {
    PluginInteractionCreator consensusPluginInteractions = new PluginInteractionCreator() {};
    sysContext.setConsensusPluginInteractions(consensusPluginInteractions);
    Assertions.assertThat(sysContext.getConsensusPluginInteractions())
        .isEqualTo(consensusPluginInteractions);
  }

  @Test
  public void getGlobalSharedObjectStorePluginState() {
    sysContext.setGlobalSharedObjectStorePluginState(blockchainAddress);
    Assertions.assertThat(sysContext.getGlobalSharedObjectStorePluginState())
        .isEqualTo(blockchainAddress);
  }

  @Test
  public void getGovernance() {
    sysContext.setGovernance(() -> "Test");
    Assertions.assertThat(sysContext.getGovernance().getChainId()).isEqualTo("Test");
  }

  @Test
  public void getFeature() {
    Assertions.assertThat(sysContext.getFeature("Test")).isNull();
    sysContext.getInvocationCreator().setFeature("Test", "ThisIsTest");
    Assertions.assertThat(sysContext.getFeature("Test")).isEqualTo("ThisIsTest");
  }

  @Test
  public void getBlockTime() {
    Assertions.assertThat(sysContext.getBlockTime()).isEqualTo(77);
    Assertions.assertThat(sysContext.getOriginalTransactionHash())
        .isEqualTo(Hash.create(stream -> stream.writeLong(77)));
  }

  @Test
  public void getBlockProductionTime() {
    long time = System.currentTimeMillis();
    Assertions.assertThat(sysContext.getBlockProductionTime())
        .isLessThanOrEqualTo(time)
        .isGreaterThan(time - 10_000);
    sysContext = new SysContractContextTest(1234, 1, blockchainAddress);
    Assertions.assertThat(sysContext.getCurrentTransactionHash())
        .isEqualTo(Hash.create(stream -> stream.writeLong(1234)));
  }

  @Test
  public void getFrom() {
    Assertions.assertThat(sysContext.getFrom()).isEqualTo(blockchainAddress);
  }

  @Test
  public void payServiceFees() {
    Assertions.assertThat(sysContext.getServiceFeesGas()).isEqualTo(0);
    Assertions.assertThat(sysContext.getServiceFeesTarget()).isNull();

    sysContext.payServiceFees(1234, blockchainAddress);

    Assertions.assertThat(sysContext.getServiceFeesGas()).isEqualTo(1234);
    Assertions.assertThat(sysContext.getServiceFeesTarget()).isEqualTo(blockchainAddress);
  }

  @Test
  public void payInfrastructureFees() {
    Assertions.assertThat(sysContext.getInfrastructureFeesGas()).isEqualTo(0);
    Assertions.assertThat(sysContext.getInfrastructureFeesTarget()).isNull();

    sysContext.payInfrastructureFees(1234, blockchainAddress);

    Assertions.assertThat(sysContext.getInfrastructureFeesGas()).isEqualTo(1234);
    Assertions.assertThat(sysContext.getInfrastructureFeesTarget()).isEqualTo(blockchainAddress);
  }

  @Test
  public void payByocFees() {
    Assertions.assertThat(sysContext.getByocFeesGas()).isNull();
    Assertions.assertThat(sysContext.getByocSymbol()).isNull();
    Assertions.assertThat(sysContext.getByocNodes()).hasSize(0);

    sysContext.registerDeductedByocFees(
        Unsigned256.create(42), "symbol", FixedList.create(List.of(blockchainAddress)));

    Assertions.assertThat(sysContext.getByocFeesGas()).isEqualTo(Unsigned256.create(42));
    Assertions.assertThat(sysContext.getByocSymbol()).isEqualTo("symbol");
    Assertions.assertThat(sysContext.getByocNodes()).containsExactly(blockchainAddress);
  }

  @Test
  public void createAccount() {
    Assertions.assertThat(sysContext.getCreatedAccount()).hasSize(0);
    sysContext.getInvocationCreator().createAccount(blockchainAddress);
    Assertions.assertThat(sysContext.getCreatedAccount()).hasSize(1).contains(blockchainAddress);
  }

  @Test
  public void shards() {
    Assertions.assertThat(sysContext.getShards()).hasSize(0);
    sysContext.getInvocationCreator().createShard("Test");
    Assertions.assertThat(sysContext.getShards()).hasSize(1).contains("Test");
    sysContext.getInvocationCreator().removeShard("Test");
    Assertions.assertThat(sysContext.getShards()).hasSize(0);
  }

  @Test
  public void updateLocalAccountPluginState() {
    Assertions.assertThat(sysContext.getUpdateLocalAccountPluginStates()).hasSize(0);
    sysContext
        .getInvocationCreator()
        .updateLocalAccountPluginState(LocalPluginStateUpdate.create(blockchainAddress, rpc));
    List<LocalPluginStateUpdate> updateLocalAccountPluginState =
        sysContext.getUpdateLocalAccountPluginStates();
    Assertions.assertThat(updateLocalAccountPluginState).hasSize(1);
    Assertions.assertThat(updateLocalAccountPluginState.get(0).getContext())
        .isEqualTo(blockchainAddress);
  }

  @Test
  public void updateContextFreeAccountPluginState() {
    Assertions.assertThat(sysContext.getShardId()).isNull();
    Assertions.assertThat(sysContext.getUpdateContextFreeAccountPluginState()).isNull();
    sysContext.getInvocationCreator().updateContextFreeAccountPluginState("Test", rpc);
    Assertions.assertThat(sysContext.getShardId()).isEqualTo("Test");
    Assertions.assertThat(sysContext.getUpdateContextFreeAccountPluginState()).isEqualTo(rpc);
  }

  @Test
  public void updateGlobalAccountPluginState() {
    Assertions.assertThat(sysContext.getUpdateGlobalAccountPluginState()).isNull();
    sysContext
        .getInvocationCreator()
        .updateGlobalAccountPluginState(GlobalPluginStateUpdate.create(rpc));
    Assertions.assertThat(sysContext.getUpdateGlobalAccountPluginState().getRpc()).isEqualTo(rpc);
  }

  @Test
  public void updateAccountPlugin() {
    Assertions.assertThat(sysContext.getUpdateAccountPluginJar()).isNull();
    Assertions.assertThat(sysContext.getUpdateAccountPluginRpc()).isNull();
    sysContext.getInvocationCreator().updateAccountPlugin(rpc, rpc);
    Assertions.assertThat(sysContext.getUpdateAccountPluginJar()).isEqualTo(rpc);
    Assertions.assertThat(sysContext.getUpdateAccountPluginRpc()).isEqualTo(rpc);
  }

  @Test
  public void updateLocalConsensusPluginState() {
    Assertions.assertThat(sysContext.getUpdateLocalConsensusPluginState()).isNull();
    sysContext
        .getInvocationCreator()
        .updateLocalConsensusPluginState(LocalPluginStateUpdate.create(blockchainAddress, rpc));
    Assertions.assertThat(sysContext.getUpdateLocalConsensusPluginState()).isNotNull();
    Assertions.assertThat(sysContext.getUpdateLocalConsensusPluginState().getRpc()).isEqualTo(rpc);
  }

  @Test
  public void updateGlobalConsensusPluginState() {
    Assertions.assertThat(sysContext.getUpdateGlobalConsensusPluginState()).isNull();
    sysContext
        .getInvocationCreator()
        .updateGlobalConsensusPluginState(GlobalPluginStateUpdate.create(rpc));
    Assertions.assertThat(sysContext.getUpdateGlobalConsensusPluginState().getRpc()).isEqualTo(rpc);
  }

  @Test
  public void updateConsensusPlugin() {
    Assertions.assertThat(sysContext.getUpdateConsensusPluginJar()).isNull();
    Assertions.assertThat(sysContext.getUpdateConsensusPluginRpc()).isNull();
    sysContext.getInvocationCreator().updateConsensusPlugin(rpc, rpc);
    Assertions.assertThat(sysContext.getUpdateConsensusPluginJar()).isEqualTo(rpc);
    Assertions.assertThat(sysContext.getUpdateConsensusPluginRpc()).isEqualTo(rpc);
  }

  @Test
  public void updateRoutingPlugin() {
    Assertions.assertThat(sysContext.getUpdateRoutingPluginJar()).isNull();
    Assertions.assertThat(sysContext.getUpdateRoutingPluginRpc()).isNull();
    sysContext.getInvocationCreator().updateRoutingPlugin(rpc, rpc);
    Assertions.assertThat(sysContext.getUpdateRoutingPluginJar()).isEqualTo(rpc);
    Assertions.assertThat(sysContext.getUpdateRoutingPluginRpc()).isEqualTo(rpc);
  }

  @Test
  public void updateGlobalSharedObjectStorePluginState() {
    Assertions.assertThat(sysContext.getUpdateGlobalSharedObjectStorePluginState()).isNull();
    sysContext
        .getInvocationCreator()
        .updateGlobalSharedObjectStorePluginState(GlobalPluginStateUpdate.create(rpc));
    Assertions.assertThat(sysContext.getUpdateGlobalSharedObjectStorePluginState().getRpc())
        .isEqualTo(rpc);
  }

  @Test
  public void updateSharedObjectStorePlugin() {
    Assertions.assertThat(sysContext.getUpdateSharedObjectStorePluginJar()).isNull();
    Assertions.assertThat(sysContext.getUpdateSharedObjectStorePluginRpc()).isNull();
    sysContext.getInvocationCreator().updateSharedObjectStorePlugin(rpc, rpc);
    Assertions.assertThat(sysContext.getUpdateSharedObjectStorePluginJar()).isEqualTo(rpc);
    Assertions.assertThat(sysContext.getUpdateSharedObjectStorePluginRpc()).isEqualTo(rpc);
  }

  @Test
  public void removeContract() {
    Assertions.assertThat(sysContext.getRemoveContract()).isNull();
    sysContext.getInvocationCreator().removeContract(blockchainAddress);
    Assertions.assertThat(sysContext.getRemoveContract()).isEqualTo(blockchainAddress);
  }

  @Test
  public void exists() {
    Assertions.assertThat(sysContext.getExists()).isNull();
    sysContext.getInvocationCreator().exists(blockchainAddress);
    Assertions.assertThat(sysContext.getExists()).isEqualTo(blockchainAddress);
  }

  @Test
  public void upgradeSystemContract() {
    Assertions.assertThat(sysContext.getUpgradeSystemContractBinderJar()).isNull();
    Assertions.assertThat(sysContext.getUpgradeSystemContractAbi()).isNull();
    Assertions.assertThat(sysContext.getUpgradeSystemContractContractJar()).isNull();
    Assertions.assertThat(sysContext.getUpgradeSystemContractRpc()).isNull();
    Assertions.assertThat(sysContext.getUpgradeSystemContractContractAddress()).isNull();
    sysContext.getInvocationCreator().upgradeSystemContract(rpc, rpc, rpc, blockchainAddress);
    Assertions.assertThat(sysContext.getUpgradeSystemContractBinderJar()).isEqualTo(rpc);
    Assertions.assertThat(sysContext.getUpgradeSystemContractContractJar()).isEqualTo(rpc);
    Assertions.assertThat(sysContext.getUpgradeSystemContractAbi()).isEqualTo(new byte[0]);
    Assertions.assertThat(sysContext.getUpgradeSystemContractRpc()).isEqualTo(rpc);
    Assertions.assertThat(sysContext.getUpgradeSystemContractContractAddress())
        .isEqualTo(blockchainAddress);
  }
}
