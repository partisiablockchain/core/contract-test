package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link StateSerializableEqualityTest}. */
public final class StateSerializableEqualityTest {

  /** {@link LargeByteArray}s can be compared. */
  @Test
  public void lba() {
    final LargeByteArray lba1 = new LargeByteArray(new byte[] {1, 2, 3});
    final LargeByteArray lba2 = new LargeByteArray(new byte[] {3, 2, 1});
    final LargeByteArray lba1Dup = new LargeByteArray(new byte[] {1, 2, 3});

    Assertions.assertThat(StateSerializableEquality.isEqualLargeByteArray(null, null)).isTrue();
    Assertions.assertThat(StateSerializableEquality.isEqualLargeByteArray(lba1, null)).isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualLargeByteArray(null, lba2)).isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualLargeByteArray(lba1, lba2)).isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualLargeByteArray(lba1, lba1)).isTrue();
    Assertions.assertThat(StateSerializableEquality.isEqualLargeByteArray(lba1, lba1Dup)).isTrue();
  }

  /** {@link FixedList}s can be compared. */
  @Test
  public void fixedList() {
    final FixedList<Integer> fixedList1 = FixedList.create(List.of(1, 2, 3));
    final FixedList<Integer> fixedList2 = FixedList.create(List.of(3, 2, 1));
    final FixedList<Integer> fixedList1Dup = FixedList.create(List.of(1, 2, 3));
    final FixedList<Integer> fixedList1Larger = fixedList1.addElement(4);

    Assertions.assertThat(StateSerializableEquality.isEqualFixedList(null, null)).isTrue();
    Assertions.assertThat(StateSerializableEquality.isEqualFixedList(fixedList1, null)).isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualFixedList(null, fixedList2)).isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualFixedList(fixedList1, fixedList2))
        .isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualFixedList(fixedList1, fixedList1))
        .isTrue();
    Assertions.assertThat(StateSerializableEquality.isEqualFixedList(fixedList1, fixedList1Dup))
        .isTrue();
    Assertions.assertThat(StateSerializableEquality.isEqualFixedList(fixedList1, fixedList1Larger))
        .isFalse();
  }

  /** {@link AvlTree}s can be compared. */
  @Test
  public void avlTree() {
    final AvlTree<Integer, Integer> avlTree1 =
        AvlTree.<Integer, Integer>create().set(1, 1).set(2, 2).set(3, 3);
    final AvlTree<Integer, Integer> avlTree2 =
        AvlTree.<Integer, Integer>create().set(3, 1).set(2, 2).set(1, 3);
    final AvlTree<Integer, Integer> avlTree1Dup =
        AvlTree.<Integer, Integer>create().set(1, 1).set(2, 2).set(3, 3);
    final AvlTree<Integer, Integer> avlTree1Larger = avlTree1.set(4, 4);

    Assertions.assertThat(StateSerializableEquality.isEqualAvlTree(null, null)).isTrue();
    Assertions.assertThat(StateSerializableEquality.isEqualAvlTree(avlTree1, null)).isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualAvlTree(null, avlTree2)).isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualAvlTree(avlTree1, avlTree2)).isFalse();
    Assertions.assertThat(StateSerializableEquality.isEqualAvlTree(avlTree1, avlTree1)).isTrue();
    Assertions.assertThat(StateSerializableEquality.isEqualAvlTree(avlTree1, avlTree1Dup)).isTrue();
    Assertions.assertThat(StateSerializableEquality.isEqualAvlTree(avlTree1, avlTree1Larger))
        .isFalse();
  }

  /** States containing {@link BlockchainPublicKey} can be compared. */
  @Test
  public void assertEqualsBlockchainPublicKey() {
    final StateBlockchainPublicKey state1 =
        new StateBlockchainPublicKey(new KeyPair(BigInteger.ONE).getPublic());
    final StateBlockchainPublicKey state2 =
        new StateBlockchainPublicKey(new KeyPair(BigInteger.TWO).getPublic());
    final StateBlockchainPublicKey state1Dup =
        new StateBlockchainPublicKey(new KeyPair(BigInteger.ONE).getPublic());

    assertEqAndNotEq(state1, state2, state1Dup);
  }

  /** States containing {@link BlsPublicKey} can be compared. */
  @Test
  public void assertEqualsBlsPublicKey() {
    final StateBlsPublicKey state1 =
        new StateBlsPublicKey(new BlsKeyPair(BigInteger.ONE).getPublicKey());
    final StateBlsPublicKey state2 =
        new StateBlsPublicKey(new BlsKeyPair(BigInteger.TWO).getPublicKey());
    final StateBlsPublicKey state1Dup =
        new StateBlsPublicKey(new BlsKeyPair(BigInteger.ONE).getPublicKey());

    assertEqAndNotEq(state1, state2, state1Dup);
  }

  /** States containing {@link BlsSignature} can be compared. */
  @Test
  public void assertEqualsBlsSignature() {
    final BlsKeyPair keyPair = new BlsKeyPair(BigInteger.ONE);
    final StateBlsSignature state1 =
        new StateBlsSignature(keyPair.sign(Hash.create(s -> s.writeInt(1))));
    final StateBlsSignature state2 =
        new StateBlsSignature(keyPair.sign(Hash.create(s -> s.writeInt(2))));
    final StateBlsSignature state1Dup =
        new StateBlsSignature(keyPair.sign(Hash.create(s -> s.writeInt(1))));

    assertEqAndNotEq(state1, state2, state1Dup);
  }

  private static void assertEqAndNotEq(
      StateSerializable state1, StateSerializable state2, StateSerializable state1Dup) {
    StateSerializableEquality.assertStatesEqual(state1, state1);
    StateSerializableEquality.assertStatesEqual(state1, state1Dup);

    Assertions.assertThatCode(() -> StateSerializableEquality.assertStatesEqual(state1, null))
        .isInstanceOf(org.opentest4j.AssertionFailedError.class);
    Assertions.assertThatCode(() -> StateSerializableEquality.assertStatesEqual(state1, state2))
        .isInstanceOf(AssertionError.class);
  }

  private record StateBlockchainPublicKey(BlockchainPublicKey key) implements StateSerializable {}

  private record StateBlsPublicKey(BlsPublicKey key) implements StateSerializable {}

  private record StateBlsSignature(BlsSignature signature) implements StateSerializable {}
}
