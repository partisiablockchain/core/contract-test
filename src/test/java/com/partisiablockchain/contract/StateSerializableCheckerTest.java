package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link StateSerializableChecker}. */
public final class StateSerializableCheckerTest {

  /** Simple states can be serialized and deserialized. */
  @Test
  public void testSimple() {
    assertStateSerializable(new UnitState());
    assertStateSerializable(new StateLong(199));
  }

  /** States containing {@link AvlTree}s can be serialized and deserialized. */
  @Test
  public void testAvl() {
    assertStateSerializable(new TestContractState(Map.of()));
    assertStateSerializable(new TestContractState(Map.of(1, new StateLong(1))));
    assertStateSerializable(
        new TestContractState(Map.of(1, new StateLong(1), 2, new StateLong(2))));
    assertStateSerializable(new TestContractState((AvlTree<Integer, StateLong>) null));
  }

  /**
   * {@link StateSerializableChecker#assertStateSerializable} throws exceptions when {@link
   * AvlTree}s cannot be (de)serialized.
   */
  @Test
  public void testAvlUnsupported() {
    Assertions.assertThatCode(
            () ->
                assertStateSerializable(
                    new TestContractState(AvlTree.<Integer, StateLong>create().set(1, null))))
        .isInstanceOf(RuntimeException.class);
    Assertions.assertThatCode(
            () ->
                assertStateSerializable(
                    new TestContractState(
                        AvlTree.<Integer, StateLong>create().set(1, null).set(2, null))))
        .isInstanceOf(RuntimeException.class);
  }

  /** States containing {@link AvlTree}s in records can be serialized and deserialized. */
  @Test
  public void testAvlInRecord() {
    assertStateSerializable(new AvlTreeState(AvlTree.<Integer, StateLong>create()));
    assertStateSerializable(
        new AvlTreeState(AvlTree.<Integer, StateLong>create().set(1, new StateLong(1))));
    assertStateSerializable(
        new AvlTreeState(
            AvlTree.<Integer, StateLong>create()
                .set(1, new StateLong(1))
                .set(2, new StateLong(2))));
    assertStateSerializable(new AvlTreeState((AvlTree<Integer, StateLong>) null));
  }

  /**
   * {@link StateSerializableChecker#assertStateSerializable} throws exceptions when {@link
   * AvlTree}s cannot be (de)serialized.
   */
  @Test
  public void testAvlInRecordUnsupported() {
    Assertions.assertThatCode(
            () ->
                assertStateSerializable(
                    new AvlTreeState(AvlTree.<Integer, StateLong>create().set(1, null))))
        .isInstanceOf(RuntimeException.class);
    Assertions.assertThatCode(
            () ->
                assertStateSerializable(
                    new AvlTreeState(
                        AvlTree.<Integer, StateLong>create().set(1, null).set(2, null))))
        .isInstanceOf(RuntimeException.class);
  }

  /** States containing {@link FixedList}s in records can be serialized and deserialized. */
  @Test
  public void testFixedList() {
    assertStateSerializable(new FixedListState(null));
    assertStateSerializable(new FixedListState(FixedList.<UnitState>create()));
    assertStateSerializable(
        new FixedListState(
            FixedList.<UnitState>create().addElement(new UnitState()).addElement(new UnitState())));
  }

  /** States containing {@link LargeByteArray}s in records can be serialized and deserialized. */
  @Test
  public void testLba() {
    assertStateSerializable(new LbaState(null));
    assertStateSerializable(new LbaState(new LargeByteArray(new byte[0])));
    assertStateSerializable(new LbaState(new LargeByteArray(new byte[10000])));
  }

  /** {@link StateSerializableChecker#writeToJson} produces JSON serialization of states. */
  @Test
  @SuppressWarnings("checkstyle:RegexpSinglelineJava") // false positive for { } in string
  public void writeToJson() {
    Assertions.assertThat(StateSerializableChecker.writeToJson(new UnitState()))
        .isEqualToIgnoringNewLines("{ }");
    Assertions.assertThat(
            StateSerializableChecker.writeToJson(
                new FixedListState(FixedList.create(List.of(new UnitState())))))
        .isEqualToIgnoringNewLines("{\n  \"ls\" : [ { } ]\n}");
  }

  /**
   * {@link StateSerializableChecker#assertStatesStableAcrossSerialization} will catch if states
   * doesn't come back right after serialization.
   */
  @Test
  public void assertStatesStableAcrossSerialization() {
    Assertions.assertThatCode(
            () ->
                StateSerializableChecker.assertStatesStableAcrossSerialization(
                    new LbaState(new LargeByteArray(new byte[0])), new LbaState(null)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining(
            "when recursively comparing field by field, but found the following difference:");
  }

  private static void assertStateSerializable(StateSerializable state) {
    final StateSerializable deserializedState =
        StateSerializableChecker.assertStateSerializable(new MemoryStateStorage(), state);
    Assertions.assertThat(deserializedState).isNotNull().isNotSameAs(state);
  }

  /** Unit state for testing. */
  @Immutable
  record UnitState() implements StateSerializable {}

  /** State containing {@link FixedList}, for testing. */
  @Immutable
  record FixedListState(FixedList<UnitState> ls) implements StateSerializable {}

  /** State containing {@link LargeByteArray}, for testing. */
  @Immutable
  record LbaState(LargeByteArray lba) implements StateSerializable {}

  /** State containing {@link AvlTree}, for testing. */
  @Immutable
  record AvlTreeState(AvlTree<Integer, StateLong> tree) implements StateSerializable {}
}
