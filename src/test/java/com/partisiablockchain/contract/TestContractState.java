package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import java.lang.reflect.Field;
import java.util.Map;

/** Example state for use in tests. */
@Immutable
public final class TestContractState implements Contract, StateSerializable {

  final AvlTree<Integer, StateLong> tree;

  TestContractState(AvlTree<Integer, StateLong> tree) {
    this.tree = tree;
  }

  @SuppressWarnings("unused")
  TestContractState() {
    this(AvlTree.create());
  }

  TestContractState(Map<Integer, StateLong> values) {
    this(AvlTree.create(values));
  }

  TestContractState invokeCallback(CallbackContext callbackContext) {
    // Do nothing if callbacks failed
    if (!callbackContext.isSuccess()) {
      return this;
    }

    // Do something otherwise
    final SafeDataInputStream resultRpc = callbackContext.results().get(0).returnValue();
    final long newValue = resultRpc.readLong();
    return new TestContractState(tree.set(9, new StateLong(newValue)));
  }

  static TestContractState invoke(TestContractState contractInformation, SafeDataInputStream rpc) {
    int type = rpc.readUnsignedByte();
    if (type == 0) {
      StateLong value = contractInformation.tree.getValue(1);
      try {
        Field field = value.getClass().getDeclaredField("value");
        field.setAccessible(true);
        field.set(value, 33);
        return contractInformation;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    } else if (type == 1) {
      return new TestContractState(contractInformation.tree.set(1, new StateLong(33)));
    } else {
      return null;
    }
  }

  @Override
  public String toString() {
    if (tree == null) {
      return "null";
    } else {
      return tree.keySet().stream()
          .map(k -> "%s = %s".formatted(k, tree.getValue(k).value()))
          .toList()
          .toString();
    }
  }
}
