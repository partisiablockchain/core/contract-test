package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateLong;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test for {@link SysContractSerialization}. */
public final class SysContractSerializationTest {

  private final TestContractState testContractState = createTestState();

  private static TestContractState createTestState() {
    return new TestContractState(
        Map.of(
            1, new StateLong(77),
            2, new StateLong(700)));
  }

  /** * Serialization for testing contract. */
  private final SysContractSerialization<TestContractState> serialization =
      new SysContractSerialization<>(MyContract.class, TestContractState.class);

  private final SysContractContext context = Mockito.mock(SysContractContext.class);

  @Test
  public void misMatchInTree() {
    assertThatCode(() -> serialization.invoke(context, testContractState, out -> out.writeByte(0)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Expecting actual:")
        .hasMessageContaining("[1 = 77, 2 = 700]")
        .hasMessageContaining("to be equal to:")
        .hasMessageContaining("[1 = 33, 2 = 700]")
        .hasMessageContaining(
            "when recursively comparing field by field, but found the following difference");
  }

  @Test
  public void create() {
    TestContractState initial = serialization.create(context, FunctionUtility.noOpConsumer());
    assertThat(initial.tree).isNotNull();
  }

  @Test
  public void successfulChange() {
    TestContractState changed =
        serialization.invoke(context, testContractState, out -> out.writeByte(1));
    assertThat(changed.tree.getValue(1).value()).isEqualTo(33L);
  }

  @Test
  public void successfulRemove() {
    TestContractState changed =
        serialization.invoke(context, testContractState, out -> out.writeByte(2));
    assertThat(changed).isNull();
  }

  @Test
  public void callbackWithGoodContract() {
    TestContractState changed =
        serialization.callback(
            context,
            callbackContextWithResults(75L, null),
            testContractState,
            out -> out.writeByte(1));
    assertThat(changed.tree.getValue(1).value()).isEqualTo(33L);
    assertThat(changed.tree.getValue(9).value()).isEqualTo(75L);
  }

  @Test
  public void callbackWithGoodContractFailedCall() {
    TestContractState changed =
        serialization.callback(
            context, callbackContextFailing(10), testContractState, out -> out.writeByte(1));
    assertThat(changed.tree.getValue(1).value()).isEqualTo(33L);
    assertThat(!changed.tree.containsKey(9)).isTrue();
  }

  @Test
  public void callbackContractForgetsToReadReturnValues() {
    assertThatCode(
            () ->
                serialization.callback(
                    context,
                    callbackContextWithResults(75L, 99L),
                    testContractState,
                    out -> out.writeByte(1)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("CallbackContext postconditions failed")
        .hasMessageContaining("Expecting empty but was: [0, 0, 0, 0, 0, 0, 0, 99]");
  }

  @Test
  public void callbackWithImpossibleContextNonEmptyFailed() {
    assertThatCode(
            () ->
                serialization.callback(
                    context,
                    callbackContextImpossibleWithNonEmptyFailed(),
                    testContractState,
                    out -> out.writeByte(1)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("CallbackContext preconditions failed")
        .hasMessageContaining("Expecting empty but was: [1, 2, 3, 4, 5, 6, 7, 8]");
  }

  @Test
  public void callbackWithImpossibleContextNull() {
    assertThatCode(
            () ->
                serialization.callback(
                    context,
                    callbackContextImpossibleWithNull(),
                    testContractState,
                    out -> out.writeByte(1)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("CallbackContext preconditions failed")
        .hasMessageContaining("Expecting actual not to be null");
  }

  @Test
  public void callbackWithImpossibleContextReusedStream() {
    assertThatCode(
            () ->
                serialization.callback(
                    context,
                    callbackContextImpossibleReusedStreams(),
                    testContractState,
                    out -> out.writeByte(1)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("CallbackContext preconditions failed")
        .hasMessageContaining("returnValue streams must be distinct");
  }

  private static final Hash SOME_HASH =
      Hash.create(
          x -> {
            x.writeInt(0xdeadbeef);
          });

  /**
   * Creates callback contexts with a given amount of results.
   *
   * @param returns A variadic number of long return values. Each return is given its own succeeding
   *     ExecutionResult. Null values are allowed, and produce ExecutionResults with null return
   *     value.
   * @return Newly created callback context.
   */
  static CallbackContext callbackContextWithResults(Long... returns) {
    final List<CallbackContext.ExecutionResult> returnsAsExecutionResults =
        Arrays.stream(returns)
            .map(
                returnValue -> {
                  if (returnValue == null) {
                    return SafeDataInputStream.createFromBytes(new byte[0]);
                  }
                  final byte[] rawBytes =
                      SafeDataOutputStream.serialize(
                          s -> {
                            s.writeLong(returnValue);
                          });
                  return SafeDataInputStream.createFromBytes(rawBytes);
                })
            .map(s -> CallbackContext.createResult(SOME_HASH, true, s))
            .toList();

    return CallbackContext.create(FixedList.create(returnsAsExecutionResults));
  }

  static CallbackContext callbackContextFailing(int numFailing) {
    final SafeDataInputStream emptyStream = SafeDataInputStream.createFromBytes(new byte[0]);
    final List<CallbackContext.ExecutionResult> executionResults =
        java.util.stream.IntStream.range(0, numFailing)
            .boxed()
            .map(i -> CallbackContext.createResult(SOME_HASH, false, emptyStream))
            .toList();
    return CallbackContext.create(FixedList.create(executionResults));
  }

  /**
   * This callback context cannot be used with serialization, as it violates the precondition for
   * failed execution results possessing empty return values.
   *
   * @return Newly created callback context.
   */
  static CallbackContext callbackContextImpossibleWithNonEmptyFailed() {
    return CallbackContext.create(
        FixedList.create(
            List.of(
                CallbackContext.createResult(
                    SOME_HASH,
                    false,
                    SafeDataInputStream.createFromBytes(new byte[] {1, 2, 3, 4, 5, 6, 7, 8})))));
  }

  /**
   * This callback context cannot be used with serialization, as it violates the precondition for
   * execution results always having return values.
   *
   * @return Newly created callback context.
   */
  static CallbackContext callbackContextImpossibleWithNull() {
    return CallbackContext.create(
        FixedList.create(List.of(CallbackContext.createResult(SOME_HASH, false, null))));
  }

  /**
   * This callback context cannot be used with serialization, as it violates the precondition for
   * execution results having unique streams (at object identity level).
   *
   * @return Newly created callback context.
   */
  static CallbackContext callbackContextImpossibleReusedStreams() {
    final var stream = SafeDataInputStream.createFromBytes(new byte[] {1, 2, 3, 4, 5, 6, 7, 8});
    return CallbackContext.create(
        FixedList.create(
            List.of(
                CallbackContext.createResult(SOME_HASH, true, stream),
                CallbackContext.createResult(SOME_HASH, true, stream))));
  }

  /**
   * {@link SysContractSerialization#verifyCallbackContextPreconditions} depends upon the equality
   * behaviour of {@link SafeDataInputStream} to check various preconditions. This test ensures that
   * this equality behaviour doesn't change without notice.
   */
  @Test
  void safeDataInputStreamEqualityBehaviourChangeCanary() {
    final byte[] bytes = new byte[] {1, 2, 3, 4, 5, 6, 7, 8};
    final SafeDataInputStream stream1 = SafeDataInputStream.createFromBytes(bytes);
    final SafeDataInputStream stream2 = SafeDataInputStream.createFromBytes(bytes);
    assertThat(stream1).isNotEqualTo(stream2);
  }

  /** Test contract. */
  public static final class MyContract extends SysContract<TestContractState> {

    @Override
    public TestContractState onCreate(SysContractContext context, SafeDataInputStream rpc) {
      return createTestState();
    }

    @Override
    public TestContractState onInvoke(
        SysContractContext context, TestContractState state, SafeDataInputStream rpc) {
      return TestContractState.invoke(state, rpc);
    }

    @Override
    public TestContractState onCallback(
        SysContractContext context,
        TestContractState state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      return TestContractState.invoke(state, rpc).invokeCallback(callbackContext);
    }
  }
}
