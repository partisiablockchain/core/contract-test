package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link SerializationChecker}. */
public final class SerializationCheckerTest {

  /** No errors are produced when {@code read} is correctly implemented. */
  @Test
  public void assertSerializeDeserialize() {
    SerializationChecker.assertSerializeDeserialize(Int::read, List.of(new Int(1), new Int(3)));
  }

  /** Errors are produced when {@code read} is incorrectly implemented. */
  @Test
  public void assertSerializeDeserializeFail() {
    Assertions.assertThatCode(
            () ->
                SerializationChecker.assertSerializeDeserialize(
                    Int::read, List.of(new Int(1001), new Int(1003))))
        .hasMessageContaining("Write/read were not symmetric");
  }

  /**
   * Example {@link DataStreamSerializable} for testing {@link SerializationChecker}.
   *
   * <p>Write method is explicitly only partially symmetric, such that we can test both the happy
   * case and the error case with a single type.
   */
  private record Int(int i) implements DataStreamSerializable {
    @Override
    public void write(SafeDataOutputStream s) {
      s.writeInt(i < 1000 ? i : 0);
    }

    /**
     * Read {@link Int} from stream.
     *
     * @param s Stream to read from
     * @return Newly read {@link Int}.
     */
    public static Int read(SafeDataInputStream s) {
      return new Int(s.readInt());
    }
  }
}
