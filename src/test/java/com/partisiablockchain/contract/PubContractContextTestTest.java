package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for {@link PubContractContextTest}. */
public final class PubContractContextTestTest {

  @Test
  public void getContractAddress() {
    Assertions.assertThat(new PubContractContextTest().getContractAddress().toString())
        .contains("010070000000000000000000000000000000000001");
  }

  @Test
  public void createEventManagerWithoutCallback() {
    PubContractContextTest context = new PubContractContextTest();
    EventCreator eventCreator = context.getInvocationCreator();
    Assertions.assertThatThrownBy(() -> eventCreator.invoke(context.getContractAddress()).send())
        .isInstanceOf(NullPointerException.class);

    Assertions.assertThat(context.getInteractions()).hasSize(0);

    eventCreator.invoke(context.getContractAddress()).withPayload(EventCreator.EMPTY_RPC).send();

    Assertions.assertThat(context.getInteractions()).hasSize(1);

    Assertions.assertThat(context.getResult()).isNull();
    context.setResult(EventCreator.EMPTY_RPC);
    Assertions.assertThat(context.getResult()).isNotNull();

    Assertions.assertThat(context.getResult()).hasSize(0);
  }

  @Test
  public void getRemoteCallsCreatorWithCallbackCost() {
    PubContractContextTest context = new PubContractContextTest();
    Assertions.assertThat(context.getRemoteCalls()).isNull();
    EventManager eventManager = context.getRemoteCallsCreator();
    Assertions.assertThatThrownBy(
            () -> eventManager.invoke(context.getContractAddress()).sendFromContract())
        .isInstanceOf(NullPointerException.class);
    Assertions.assertThat(context.getInteractions()).hasSize(0);
    Assertions.assertThat(context.getRemoteCalls()).isNotNull();
    Assertions.assertThat(context.getResult()).isNull();

    context
        .getRemoteCallsCreator()
        .invoke(context.getContractAddress())
        .withPayload(EventCreator.EMPTY_RPC)
        .sendFromContract();

    Assertions.assertThat(context.getInteractions()).hasSize(0);
    Assertions.assertThat(context.getRemoteCalls()).isNotNull();
    Assertions.assertThat(context.getResult()).isNull();

    ContractEventGroup actual = context.getRemoteCalls();
    Assertions.assertThat(actual.callbackRpc).isNull();
    Assertions.assertThat(actual.callbackCost).isNull();
    Assertions.assertThat(actual.contractEvents).hasSize(1);
    ContractEventInteraction interaction = (ContractEventInteraction) actual.contractEvents.get(0);
    Assertions.assertThat(interaction.originalSender).isFalse();

    eventManager.registerCallback(EventManager.EMPTY_RPC, 1234);
    actual = context.getRemoteCalls();

    Assertions.assertThat(actual.callbackRpc).isEqualTo(new byte[0]);
    Assertions.assertThat(actual.callbackCost).isEqualTo(1234L);

    context.setResult(stream -> stream.writeLong(1234));
    Assertions.assertThat(context.getRemoteCalls()).isNull();
    Assertions.assertThat(context.getResult())
        .hasSize(8)
        .isEqualTo(new byte[] {0, 0, 0, 0, 0, 0, 4, -46});
  }

  @Test
  public void getRemoteCallsCreatorWithCallbackNoCost() {
    PubContractContextTest context = new PubContractContextTest();
    Assertions.assertThat(context.getRemoteCalls()).isNull();
    EventManager eventManager = context.getRemoteCallsCreator();

    context
        .getRemoteCallsCreator()
        .invoke(context.getContractAddress())
        .withPayload(EventCreator.EMPTY_RPC)
        .sendFromContract();
    eventManager.registerCallbackWithCostFromRemaining(EventManager.EMPTY_RPC);
    ContractEventGroup actual = context.getRemoteCalls();

    Assertions.assertThat(actual.callbackRpc).isEqualTo(new byte[0]);
    Assertions.assertThat(actual.callbackCost).isNull();
  }

  @Test
  public void getBlockTime() {
    Assertions.assertThat(
            new PubContractContextTest(
                    1,
                    1,
                    BlockchainAddress.fromString("000070000000000000000000000000000000000001"))
                .getBlockTime())
        .isEqualTo(1);
    Assertions.assertThat(
            new PubContractContextTest(
                    1,
                    123456789,
                    BlockchainAddress.fromString("000070000000000000000000000000000000000001"))
                .getBlockTime())
        .isEqualTo(123456789);
  }

  @Test
  public void getBlockProductionTime() {
    Assertions.assertThat(
            new PubContractContextTest(
                    1,
                    1,
                    BlockchainAddress.fromString("000070000000000000000000000000000000000001"))
                .getBlockProductionTime())
        .isEqualTo(1);
    Assertions.assertThat(
            new PubContractContextTest(
                    123456789,
                    1,
                    BlockchainAddress.fromString("000070000000000000000000000000000000000001"))
                .getBlockProductionTime())
        .isEqualTo(123456789);
  }

  @Test
  public void getFrom() {
    Assertions.assertThat(new PubContractContextTest().getFrom().toString())
        .contains("000070000000000000000000000000000000000001");
  }

  @Test
  public void getCurrentTransactionHash() {
    Assertions.assertThat(
            new PubContractContextTest(
                    1,
                    1,
                    BlockchainAddress.fromString("000070000000000000000000000000000000000001"))
                .getCurrentTransactionHash())
        .isEqualTo(
            Hash.fromString("cd2662154e6d76b2b2b92e70c0cac3ccf534f9b74eb5b89819ec509083d00a50"));
  }

  @Test
  public void getOriginalTransactionHash() {
    Assertions.assertThat(
            new PubContractContextTest(
                    1,
                    1,
                    BlockchainAddress.fromString("000070000000000000000000000000000000000001"))
                .getOriginalTransactionHash())
        .isEqualTo(
            Hash.fromString("cd2662154e6d76b2b2b92e70c0cac3ccf534f9b74eb5b89819ec509083d00a50"));
  }
}
