package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.serialization.StateLong;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test of {@link PubContractSerialization}. */
public final class PubContractSerializationTest {

  private final TestContractState testContractState = createTestState();

  private static TestContractState createTestState() {
    return new TestContractState(
        Map.of(
            1, new StateLong(77),
            2, new StateLong(700)));
  }

  /** * Serialization for testing contract. */
  private final PubContractSerialization<TestContractState> serialization =
      new PubContractSerialization<>(MyContract.class, TestContractState.class);

  private final PubContractContext context = Mockito.mock(PubContractContext.class);

  @Test
  public void create() {
    TestContractState initial = serialization.create(context, FunctionUtility.noOpConsumer());
    assertThat(initial.tree).isNotNull();
  }

  @Test
  public void misMatchInTree() {
    Assertions.setMaxStackTraceElementsDisplayed(100);
    assertThatCode(() -> serialization.invoke(context, testContractState, out -> out.writeByte(0)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Expecting actual:")
        .hasMessageContaining("[1 = 77, 2 = 700]")
        .hasMessageContaining("to be equal to:")
        .hasMessageContaining("[1 = 33, 2 = 700]")
        .hasMessageContaining(
            "when recursively comparing field by field, but found the following difference");
  }

  @Test
  public void successfulRemove() {
    TestContractState changed =
        serialization.invoke(context, testContractState, out -> out.writeByte(2));
    assertThat(changed).isNull();
  }

  @Test
  public void successfulChange() {
    TestContractState changed =
        serialization.invoke(context, testContractState, out -> out.writeByte(1));
    assertThat(changed.tree.getValue(1).value()).isEqualTo(33L);
  }

  @Test
  public void callbackWithGoodContract() {
    TestContractState changed =
        serialization.callback(
            context,
            SysContractSerializationTest.callbackContextWithResults(75L, null),
            testContractState,
            out -> out.writeByte(1));
    assertThat(changed.tree.getValue(1).value()).isEqualTo(33L);
    assertThat(changed.tree.getValue(9).value()).isEqualTo(75L);
  }

  @Test
  public void callbackContractForgetsToReadReturnValues() {
    assertThatCode(
            () ->
                serialization.callback(
                    context,
                    SysContractSerializationTest.callbackContextWithResults(75L, 99L),
                    testContractState,
                    out -> out.writeByte(1)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("CallbackContext postconditions failed")
        .hasMessageContaining("Expecting empty but was: [0, 0, 0, 0, 0, 0, 0, 99]");
  }

  @Test
  public void callbackWithImpossibleContextNonEmptyFailed() {
    assertThatCode(
            () ->
                serialization.callback(
                    context,
                    SysContractSerializationTest.callbackContextImpossibleWithNonEmptyFailed(),
                    testContractState,
                    out -> out.writeByte(1)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("CallbackContext preconditions failed")
        .hasMessageContaining("Expecting empty but was: [1, 2, 3, 4, 5, 6, 7, 8]");
  }

  @Test
  public void callbackWithImpossibleContextNull() {
    assertThatCode(
            () ->
                serialization.callback(
                    context,
                    SysContractSerializationTest.callbackContextImpossibleWithNull(),
                    testContractState,
                    out -> out.writeByte(1)))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("CallbackContext preconditions failed")
        .hasMessageContaining("Expecting actual not to be null");
  }

  /** Test contract. */
  public static final class MyContract extends PubContract<TestContractState> {

    @Override
    public TestContractState onCreate(PubContractContext context, SafeDataInputStream rpc) {
      return createTestState();
    }

    @Override
    public TestContractState onInvoke(
        PubContractContext context, TestContractState state, SafeDataInputStream rpc) {
      return TestContractState.invoke(state, rpc);
    }

    @Override
    public TestContractState onCallback(
        PubContractContext context,
        TestContractState state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      return TestContractState.invoke(state, rpc).invokeCallback(callbackContext);
    }
  }
}
